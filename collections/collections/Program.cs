﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            var ListOfSuperHeroes2 = new List<SuperHero>();
            var tempHero1 = new SuperHero();
            tempHero1.age = 30;
            tempHero1.brand = "Marvel";
            tempHero1.name = "Deadpool";
            tempHero1.power = "sass";
            ListOfSuperHeroes = GetFiveSuperHeroes();
            var tempHero2 = new SuperHero();
            tempHero2.age = 42;
            tempHero2.brand = "Marvel";
            tempHero2.name = "Dr Strange";
            tempHero2.power = "Time Travel";
            ListOfSuperHeroes2.Add(tempHero1);
            ListOfSuperHeroes2.Add(tempHero2);
            var FinalList = new List<SuperHero>();
            FinalList.AddRange(ListOfSuperHeroes);
            FinalList.AddRange(ListOfSuperHeroes2);

            // ShowSuperHeroes(ListOfSuperHeroes);
            // ShowSuperHeroes(ListOfSuperHeroes2);

            ShowSuperHeroes(FinalList);

            int age = 10;
            SuperHero superHero = findByAge(age,FinalList);
            if (returnHero1 == null)
                Console.WriteLine("No value to return");
            else
                String name = "Wonderwomen";
            SuperHeroreturnHero2=findByName()
            Console.ReadLine();
        }

        private static SuperHero findByAge(int age, List<SuperHero> finalList)
        {
            SuperHero temptHero = findByAge(age,finalList);
            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            temptHero=finalList.Select(x=>x).Where(x => x.age == 10).FirstOrDefault();
            return temptHero ;
        }

        private static SuperHero findByName(String name, List<SuperHero> finalList)
        {

            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            var localHero = finalList.Select(x => x).Where(x => x.name == name).FirstOrDefault();

            return localHero;
        }
        private static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();

            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }

        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes.
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes.
            var tempHero = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Superman";
            power = "Superhuman Strength";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Wonderwoman";
            power = "Strength";
            brand = "DC";
            age = 28;
            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Captain America";
            power = "Shield";
            brand = "Marvel";
            age = 52;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Ironman";
            power = "Armor";
            brand = "Marvel";
            age = 44;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Hulk";
            power = "Strength";
            brand = "Marvel";
            age = 54;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list.
            tempList.Add(tempHero);


            return tempList;
        }
    }
}