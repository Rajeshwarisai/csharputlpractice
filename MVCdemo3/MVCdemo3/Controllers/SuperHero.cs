﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCdemo3.Controllers
{
    public class SuperHero
    {
        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age { get; set; }
    }
}
    
