﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            
            ViewBag.Message = "jay like sweets";
            ViewBag.Message1 = "I do like sweets";
            ViewBag.Message2 = "dogs are so cute";
            ViewBag.Message3 = "yyyyyyyyyyyyy";

            return View();
        }

        public ActionResult ShowList()
        {
            ViewBag.Message = "Your shwlist page.";

            return View();
        }
    }
}