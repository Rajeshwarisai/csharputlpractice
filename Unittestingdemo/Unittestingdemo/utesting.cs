﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unittestingdemo
{
    class utesting
    {
        ///unit test for code reuse.
        //copy paste this code file to build new unit tests quickly.
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void TestSample1()
            {
                var actual = true;
                var expected = true;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample2()
            {
                var actual = true;
                var expected = false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample3()
            {

                var input1 = 210.36;
                var input2 = 310.28;
                var actual = Program.SumOfTwoNumbers(input1, input2);
                var expected = 520.64;
                Assert.AreEqual(expected, actual);
            }

            [TestMethod]
            public void TestSample4()
            {

                var input1 = 210.36;
                var input2 = 310.28;
                var actual = Program.SumOfTwoNumbers(input1, input2);
                var expected = 520.64;
                Assert.AreEqual(expected, actual);
            }

            [TestMethod]

            public void TestSample5()
            {

                var input1 = 5263;
                var input2 = 6253;
                var actual = Program.SumOfTwoNumbers(input1, input2);
                var expected = 11516;
                Assert.AreEqual(expected, actual);
            }

            [TestMethod]

            public void TestSampleSix()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "DC";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value. 
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.FindByAge( ageofhero, ListOfSuperHeroes);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value. 
                bool expected = true;
                //in the beginning set actual value to false. 
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age &&
                   expectedhero.brand == actualhero.brand &&
                   expectedhero.name == actualhero.name &&
                   expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly. 
                    //set actual bool to true. 
                    actual = true;
                }

                Assert.AreEqual(expected, actual);
            }
            //this one fails on purpose.
           [TestMethod]
           public void TestSampleSeven()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "Marvel";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value.
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.ReturnSpecificHeroBasedOnAge(ListOfSuperHeroes, ageofhero);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value.
                bool expected = true;
                //in the beginning set actual value to false.
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age &&
                    expectedhero.brand == actualhero.brand &&
                    expectedhero.name == actualhero.name &&
                    expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly.
                    //set actual bool to true.
                    actual = true;
                }

                Assert.AreEqual(expected, actual);

            }
            Message Input


Message #csharpdiscussion

            
        }
    }
    }

            
        




