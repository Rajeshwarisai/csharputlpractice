﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unittestingdemo
{
    class Program
    {
        static void Main(string[] args)
        {
           double num1 = 10;
            double num2 = 20;
            double resultOfaddition = 0.0;
            resultOfaddition = SumOfTwoNumbers(num1,num2);

           
            var ListOfSuperHeroes = new List<SuperHero>();
            ListOfSuperHeroes = GetFiveSuperHeroes();
            SuperHero temphero = FindByAge(30, ListOfSuperHeroes);

            Console.ReadLine();
        }

        public static SuperHero FindByAge(int age, List<SuperHero> finalList)
        {
            SuperHero temphero = new SuperHero();
            temphero = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();


            return temphero;
        }
        
        public  static double SumOfTwoNumbers(double num1, double num2)
        {
            double result = num1 + num2;
            return result;
        }

       public static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 30;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Jay";
            power = "Sweets";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Antman";
            power = "Technique";
            brand = "DC";
            age = 40;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Ironman";
            power = "Power";
            brand = "DC";
            age = 52;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Spiderman";
            power = "Technique";
            brand = "DC";
            age = 35;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Wonerwomen";
            power = "Skills";
            brand = "DC";
            age = 42;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }

    }
}

