﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unittestingdemo
{
    class SuperHero
    {
        private string v1;
        private string v2;
        private string v3;
        private int v4;

        public SuperHero(string v1, string v2, string v3, int v4)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            this.v4 = v4;
        }

        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age { get; set; }
    }
}
